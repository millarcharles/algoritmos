

preguntas_paciente = [
    {
        'pregunta': 'nombre',
        'print': 'Ingrese el Nombre ',
        'tabla': 'Nombre:',


    }, {
        'pregunta': 'apellido',
        'print': 'Ingrese el apellido ',
        'tabla': 'Apellido:',


    }, {
        'pregunta': 'rut',
        'print': 'Ingrese el RUT ',
        'tabla': 'Rut:'
    }, {
        'pregunta': 'estado_civil',
        'print': 'Ingrese el Estado Civil ',
        'tabla': 'Estado Civil'
    }, {
        'pregunta': 'domicilio',
        'print': 'Ingrese el domicilio',
        'tabla': 'Domicilio:'
    }, {
        'pregunta': 'telefono',
        'print': 'Ingrese el Telefono de contacto',
        'tabla': 'Fono:'
    }, {
        'pregunta': 'acompanado',
        'print': 'El Paciente viene acompañado? /(SI/NO/)',
        'tabla': 'Asiste acompañado:'
    }, {
        'pregunta': 'urgencia',
        'print': 'Nivel de Urgencia /(1-3/)',
        'tabla': 'Nivel de urgencia :'
    }, {
        'pregunta': 'sexo',
        'print': 'Sexo del paciente',
        'tabla': 'Sexo :'
    }, {
        'pregunta': 'edad',
        'print': 'Edad del paciente',
        'tabla': 'Edad :'
    }, {
        'pregunta': 'grupo_sanguineo',
        'print': 'Grupo sanguineo del paciente',
        'tabla': 'Grupo sanguineo :'
    },
]

##Preguntas sobre el acompañante

preguntas_acompanante = [
    {
        'pregunta': 'nombre_acompañante',
        'print': 'Nombre del acompañante: ',
        'tabla': 'Nombre:'
    }, {
        'pregunta': 'apellido_acompañante',
        'print': 'Apellido del acompañante: ',
        'tabla': 'Apellido:'
    }, {
        'pregunta': 'rut_acompañante',
        'print': 'RUT del acompañante: ',
        'tabla': 'RUT:'
    }, {
        'pregunta': 'grado_parentesco',
        'print': 'Indique el Grado de Parentesco: ',
        'tabla': 'Grado de Parentesco:'
    }, {
        'pregunta': 'telefono_acompañante',
        'print': 'Ingrese el Telefono: ',
        'tabla': 'Fono:'
    }]

##
##IFORMACION DE ATENCION
info_atencion = [
    {
        'pregunta': 'nombre_medico',
        'print': 'Nombre del medico que atiende al paciente: ',
        'tabla': 'Nombre:'
    }, {
        'pregunta': 'especialidad_medico',
        'print': 'Especialidad del medico: ',
        'tabla': 'Apellido:'
    }, {
        'pregunta': 'sintomas_detectados',
        'print': 'que sintomas fueron detectados: ',
        'tabla': 'Sintomas detectados:'
    }, {
        'pregunta': 'diagnostico_medico',
        'print': 'Cual es el diagnostico: ',
        'tabla': 'Diagnóstico:'
    }, {
        'pregunta': 'reposo',
        'print': 'Indica reposo para el paciente? ',
        'tabla': 'Reposo:'
    }, {
        'pregunta': 'dias_reposo',
        'print': 'Cuantos dias? ',
        'tabla': 'Cantidad de dias:'
    }, {
        'pregunta': 'asigna_medicamento',
        'print': 'Asigna algun medicamento al paciente? ',
        'tabla': 'Médico Asigna medicamento:'
    }, {
        'pregunta': 'nombre_medicamento',
        'print': 'Que medicamento deberia tomar? ',
        'tabla': 'Nombre de medicamento:'
    }, {
        'pregunta': 'dosis_medicamento',
        'print': 'Que dosis deberia tomar? ',
        'tabla': 'Dosis:'
    }, {
        'pregunta': 'dias_medicamento',
        'print': 'Por cuantos dias?',
        'tabla': 'Cantidad de días:'
    }]

##Vatiable respuestas, almacena los datos colectados
def formateo_rut(rut):
    for _d in rut:
        rut = rut[-1:]
        dv = rut[rut.length]



def paciente_duplicado(rut):
    for _m in pacientes:
        if _m['rut'] == rut:
            print(f'el paciente con documento {rut} ya se encuentra en la base de datos')
def pedir_nombre(cual,set):
    for n in set:
        if n['pregunta'] == cual:
            print(f"{n['print']}")
            respuesta = input()
            respuestas[n['pregunta']] = respuesta

def pedir_rut(set):
    for _n in set:
        if _n['pregunta'] == 'rut':
            print(f"{_n['print']}")
            respuesta = input()
            respuestas[f"{_n['pregunta']}"] = respuesta
            for _m in pacientes:
                if _m['rut'] == respuesta:
                    paciente_duplicado(_m['rut'])
                else:
                    respuestas['pregunta'] = respuesta


respuestas = {}
pacientes = []

def ingresar_paciente():
    pedir_nombre('nombre',preguntas_paciente)
    pedir_nombre('apellido',preguntas_paciente)
    pedir_rut(preguntas_paciente)
    pacientes.append(respuestas)
    print(pacientes)

ingresar_paciente()
ingresar_paciente()