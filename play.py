import os, datetime, random
#fx formatear rut:
class Printer():
    def imprimir_titulo_ficha(self,_p):
        print(
            f"\t\t*************************************************************\n"
            f"\t\t             ***  Centro medico DUOC UC ***\n"
            f"\t\t______________________________________________________________\n"
            f"\t\t                      FICHA Nº XXXX                           \n"
            f"\t\t____________________________________________________________\n"

            f"\t\tFecha de atención:XX/XX/XXXX    |    Hora de Atención: HH:mm\n"
            f"________________________________________________________________________\n")

    def imprimir_info_paciente(self,_p):
        print(f"\t\tIDENTIFICACION DE PACIENTE"
              f"_________________________________________________________________________________________\n"
              f"\t\tNombre: {_p['nombre']}                                  |   Nivel de urgencia:   {_p['urgencia']}\n"
              f"\t\tApellido: {_p['nombre']}                                |   Nivel de urgencia:   {_p['urgencia']}\n"
              f"\t\tRUT: {formateo_rut(_p['rut'])}                          |   Nivel de urgencia:   {_p['urgencia']}\n"
              f"\t\tEstado Civil: {_p['nombre']}                                  |   "
              f"\t\tDomicilio: {_p['nombre']}                                    "

              f"\t\tFono: {_p['nombre']}            "
              )

    def imprimir_info_acompanante(self,_p):
        if _p['tiene_compañia']:
            print(
                f"\t\tAsiste acompañado:         SI\n"
                f"_______________________________________________________\n"
                f"INFORMACION DE ACOMPAÑANTE\n"
                f"_______________________________________________________\n"
                f"Nombre: {_p['nombre_acompanante']}\n"
                f"Apellido: {_p['apellido_acompanante']}\n"
                f"Rut: {formateo_rut(_p['rut_acompanante'])}\n"
                f"Grado de parentesco: {_p['grado_parentesco']}|Fono:  {_p['telefono_acompanante']}"
            )
        else:
            print(
                f"Asiste acompañado:             NO"
            )

    def imprimir_info_atencion(self,_p):
        print(
            f"____________________________\n"
            f"INFORMACION DE ATENCION\n"
            f"____________________________\n"
            f"Nombre médico: {_p['nombre_medico']}\n"
            f"Especialidad: {_p['especialidad_medico']}\n"
            f"Sintomas detectados: {_p['sintomas_detectados']}\n")

        if _p['indica_reposo']:
            print(f"Reposo:   SI"
                  f"Cantidad de dias: {_p['dias_reposo']}")
        else:
            print(f"Reposo:     NO"
                  )

    def imprimir_info_medicamento(self,_p):
        print(f'____________________________________________\n'
              f'INFORMACIÓN DE MEDICAMENTO'
              f"\n__________________________________________")
        if _p['indica_medicamento']:
            print(f"Medico Asigna medicamento:                 SI")
            print(f"Nombre de medicamento:       {_p['nombre_medicamento']}")
            print(f"Dosis:       {_p['dosis_medicamento']}")
            print(f"Cantidad de días:       {_p['dias_medicamento']}")
        else:
            print(f"Medico Asigna medicamento:                 NO")

    def imprimir_todo(self,_p):
        imprimir_titulo_ficha(_p)
        imprimir_info_paciente(_p)
        imprimir_info_acompanante(_p)
        imprimir_info_atencion(_p)
        imprimir_info_medicamento(_p)


def formateo_rut(rut):

    ru = rut[:-1]
    dv = rut[-1]
    ur = ru[::-1]
    partes = [ur[i:i+3] for i in range(0, len(ru), 3)]
    new = '.'.join(partes)
    return (f"{new[::-1]}-{dv}")



pacientes = [{'rut': '123', 'nombre': 'ca', 'apellido': 'mi', 'estado_civil': 'soltero', 'domicilio': 'ma', 'telefono': 'si', 'acompanado': 'si', 'tiene_compañia': True, 'urgencia': '2', 'sexo': 'male', 'edad': '123', 'nombre_acompanante': 'ca', 'apellido_acompanante': 'pa', 'rut_acompanante': '18720', 'grado_parentesco': 'hermanatres', 'telefono_acompanante': '13321', 'nombre_medico': 'jorge', 'especialidad_medico': 'medico', 'sintomas_detectados': 'tos', 'reposo': 'si', 'indica_reposo': True, 'medicamento': 'si', 'indica_medicamento': True, 'dias_reposo': '3', 'nombre_medicamento': 'para', 'dosis_medicamento': '50', 'dias_medicamento': '3'}, {'rut': '123', 'nombre': 'ca', 'apellido': 'mi', 'estado_civil': 'soltero', 'domicilio': 'ma', 'telefono': 'si', 'acompanado': 'si', 'tiene_compañia': True, 'urgencia': '2', 'sexo': 'male', 'edad': '123', 'nombre_acompanante': 'ca', 'apellido_acompanante': 'pa', 'rut_acompanante': '18720', 'grado_parentesco': 'hermanatres', 'telefono_acompanante': '13321', 'nombre_medico': 'jorge', 'especialidad_medico': 'medico', 'sintomas_detectados': 'tos', 'reposo': 'si', 'indica_reposo': True, 'medicamento': 'si', 'indica_medicamento': True, 'dias_reposo': '3', 'nombre_medicamento': 'para', 'dosis_medicamento': '50', 'dias_medicamento': '3'}]
for _p in pacientes:
    print(_p)
    os.system('clear')
    w=30
    printer = Printer()


def imprimir_titulo_ficha(_p):
    print(
    f"\t\t*************************************************************\n"
    f"\t\t             ***  Centro medico DUOC UC ***\n"
    f"\t\t______________________________________________________________\n"
    f"\t\t                      FICHA Nº XXXX                           \n"
    f"\t\t____________________________________________________________\n"
    
    f"\t\tFecha de atención:XX/XX/XXXX    |    Hora de Atención: HH:mm\n"
    f"________________________________________________________________________\n"
   )
def imprimir_info_paciente(_p):
    print( f"\t\tIDENTIFICACION DE PACIENTE"
    f"_________________________________________________________________________________________\n"
    f"\t\tNombre: {_p['nombre']}                                  |   Nivel de urgencia:   {_p['urgencia']}\n"
    f"\t\tApellido: {_p['nombre']}                                |   Nivel de urgencia:   {_p['urgencia']}\n"                       
    f"\t\tRUT: {formateo_rut(_p['rut'])}                          |   Nivel de urgencia:   {_p['urgencia']}\n"
    f"\t\tEstado Civil: {_p['nombre']}                                  |   Nivel de urgencia:   {_p['urgencia']}\n"
    f"\t\tDomicilio: {_p['nombre']}                                  |   Nivel de urgencia:   {_p['urgencia']}\n"
    f"\t\tFono: {_p['nombre']}                                  "

)
def imprimir_info_acompanante(_p):
    if _p['tiene_compañia']:
        print(
            f"\t\tAsiste acompañado:         SI\n"
            f"_______________________________________________________\n"
            f"INFORMACION DE ACOMPAÑANTE\n"
            f"_______________________________________________________\n"
            f"Nombre: {_p['nombre_acompanante']}\n"
            f"Apellido: {_p['apellido_acompanante']}\n"
            f"Rut: {formateo_rut(_p['rut_acompanante'])}\n"
            f"Grado de parentesco: {_p['grado_parentesco']}|Fono:  {_p['telefono_acompanante']}"
        )
    else: print(
        f"Asiste acompañado:             NO"
    )
def imprimir_info_atencion(_p):
    print(
        f"____________________________\n"
        f"INFORMACION DE ATENCION\n"
        f"____________________________\n"
        f"Nombre médico: {_p['nombre_medico']}\n"
        f"Especialidad: {_p['especialidad_medico']}\n"
        f"Sintomas detectados: {_p['sintomas_detectados']}\n")

    if _p['indica_reposo']:
        print(f"Reposo:   SI"
        f"Cantidad de dias: {_p['dias_reposo']}")
    else:
        print(f"Reposo:     NO"
                  )
def imprimir_info_medicamento(_p):
    print(f'____________________________________________\n'
          f'INFORMACIÓN DE MEDICAMENTO'
          f"\n__________________________________________")
    if _p['indica_medicamento']:
        print(f"Medico Asigna medicamento:                 SI")
        print(f"Nombre de medicamento:       {_p['nombre_medicamento']}")
        print(f"Dosis:       {_p['dosis_medicamento']}")
        print(f"Cantidad de días:       {_p['dias_medicamento']}")
    else:
        print(f"Medico Asigna medicamento:                 NO")
def imprimir_ficha_completa(_p):
    imprimir_titulo_ficha(_p)
    imprimir_info_paciente(_p)
    imprimir_info_acompanante(_p)
    imprimir_info_atencion(_p)
    imprimir_info_medicamento(_p)








printer = Printer()

