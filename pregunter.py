
##Preguntas sobre el paciente

preguntas_paciente = [
    {
        'pregunta': 'nombre',
        'print': 'Ingrese el Nombre ',
        'tabla' : 'Nombre:'
    },{
        'pregunta': 'apellido',
        'print': 'Ingrese el apellido ',
        'tabla' : 'Apellido:'
    },{
        'pregunta': 'rut',
        'print': 'Ingrese el RUT ',
        'tabla' : 'Rut:'
    },{
        'pregunta': 'estado_civil',
        'print': 'Ingrese el Estado Civil ',
        'tabla' : 'Estado Civil'
    },{
        'pregunta': 'domicilio',
        'print': 'Ingrese el domicilio',
        'tabla' : 'Domicilio:'
    },{
        'pregunta': 'telefono',
        'print': 'Ingrese el Telefono de contacto',
        'tabla' : 'Fono:'
    },{
        'pregunta': 'acompanado',
        'print': 'El Paciente viene acompañado? /(SI/NO/)',
        'tabla' : 'Asiste acompañado:'
    },{
        'pregunta': 'urgencia',
        'print': 'Nivel de Urgencia /(1-3/)',
        'tabla' : 'Nivel de urgencia :'
    },{
        'pregunta': 'sexo',
        'print': 'Sexo del paciente',
        'tabla' : 'Sexo :'
    },{
        'pregunta': 'edad',
        'print': 'Edad del paciente',
        'tabla' : 'Edad :'
    },{
        'pregunta': 'grupo_sanguineo',
        'print': 'Grupo sanguineo del paciente',
        'tabla' : 'Grupo sanguineo :'
    },
]




##Preguntas sobre el acompañante

preguntas_acompanante = [
    {
        'pregunta': 'nombre_acompañante',
        'print': 'Nombre del acompañante: ',
        'tabla': 'Nombre:'
    },{
        'pregunta': 'apellido_acompañante',
        'print': 'Apellido del acompañante: ',
        'tabla': 'Apellido:'
    },{
        'pregunta': 'rut_acompañante',
        'print': 'RUT del acompañante: ',
        'tabla': 'RUT:'
    },{
        'pregunta': 'grado_parentesco',
        'print': 'Indique el Grado de Parentesco: ',
        'tabla': 'Grado de Parentesco:'
    },{
        'pregunta': 'telefono_acompañante',
        'print': 'Ingrese el Telefono: ',
        'tabla': 'Fono:'
    }]

##
##IFORMACION DE ATENCION
info_atencion = [
    {
        'pregunta': 'nombre_medico',
        'print': 'Nombre del medico que atiende al paciente: ',
        'tabla': 'Nombre:'
    },{
        'pregunta': 'especialidad_medico',
        'print': 'Especialidad del medico: ',
        'tabla': 'Apellido:'
    },{
        'pregunta': 'sintomas_detectados',
        'print': 'que sintomas fueron detectados: ',
        'tabla': 'Sintomas detectados:'
    },{
        'pregunta': 'diagnostico_medico',
        'print': 'Cual es el diagnostico: ',
        'tabla': 'Diagnóstico:'
    },{
        'pregunta': 'reposo',
        'print': 'Indica reposo para el paciente? ',
        'tabla': 'Reposo:'
    },{
        'pregunta': 'dias_reposo',
        'print': 'Cuantos dias? ',
        'tabla': 'Cantidad de dias:'
    },{
        'pregunta': 'asigna_medicamento',
        'print': 'Asigna algun medicamento al paciente? ',
        'tabla': 'Médico Asigna medicamento:'
    },{
        'pregunta': 'nombre_medicamento',
        'print': 'Que medicamento deberia tomar? ',
        'tabla': 'Nombre de medicamento:'
    },{
        'pregunta': 'dosis_medicamento',
        'print': 'Que dosis deberia tomar? ',
        'tabla': 'Dosis:'
    },{
        'pregunta': 'dias_medicamento',
        'print': 'Por cuantos dias?',
        'tabla': 'Cantidad de días:'
    }]

##Vatiable respuestas, almacena los datos colectados

respuestas = {}

#fx preguntar, itera sobre la lista de preguntas preguntando cada una,  
##     almacenando respuestas en el dict respuestas
def preguntar(preguntas):
    for pregunta in preguntas:
        print(pregunta['print'])
        respuestas[pregunta['pregunta']] = input()
    
#imprime el set de preguntas, junto con los datos almacenados
def impresora(set_preguntas,ficha):
    for preg in set_preguntas:
        w=30
        att = f"{preg['tabla']}"
        dato = f"{ficha[preg['pregunta']]}"
        print(f"{att.ljust(w)} {dato.rjust(w)}")


preguntar(preguntas_paciente)

preguntar(preguntas_acompanante)
preguntar(info_atencion)

              
              
answers = {'nombre': 'carlos', 'apellido': 'millar', 'rut': '1872', 'estado_civil': 'ca', 'domicilio': 'micasa', 'telefono': '2134213', 'acompanado': 'si', 'urgencia': '2', 'sexo': 'masc', 'edad': '123', 'grupo_sanguineo': 'th123', 'nombre_acompañante': '', 'apellido_acompañante': '', 'rut_acompañante': 'ca', 'grado_parentesco': 'pareja', 'telefono_acompañante': '2311', 'nombre_medico': 'cas', 'especialidad_medico': 'tillo', 'sintomas_detectados': 'tos, diarrea, caca', 'diagnostico_medico': 'se va a morir', 'reposo': 'no', 'dias_reposo': '7', 'asigna_medicamento': 'si', 'nombre_medicamento': 'paracetamol', 'dosis_medicamento': '5kilos', 'dias_medicamento': '100'}


print(f'\n\n\n')
print(f'INFO DE PACIENTE'.center(30,"#"))       
impresora(preguntas_paciente,respuestas)
print(f'\n')
print(f"INFO DE ACOMPAÑANTE".center(30,"#"))
impresora(preguntas_acompanante,respuestas)
print(f'\n')
print(f' INFO DE ATENCION'.center(30,"#"))
impresora(info_atencion,respuestas)


