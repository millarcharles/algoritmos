import os
from datetime import datetime as dt
from datetime import time, date







##class printer, almacena las functiones que dan formato a la ficha
class Printer():
    def imprimir_titulo_ficha(self,_p):
        print(
            f"\t\t*************************************************************\n"
            f"\t\t\t          ***  Centro medico DUOC UC ***\n"
            f"\t\t______________________________________________________________\n"
            f"\t\t                      FICHA Nº XXXX                           \n"
            f"\t\t______________________________________________________________\n"

            f"\t\tFecha de atención: {f_str}    |    Hora de Atención: {h_str}\n"
            f"\t\t______________________________________________________________\n")
    def imprimir_info_paciente(self,_p):
        print(f"\t\t\t\t\tINFORMACION PACIENTE\n"
              f"\t\t______________________________________________________________\n"
              f"\t\tNombre: {_p['nombre']}\t\t      \t\tNivel de urgencia:   {_p['urgencia']}\n"
              f"\t\tApellido: {_p['nombre']}                            RUT: {formateo_rut(_p['rut'])}\n"
              f"\t\tEstado Civil: {_p['nombre']}"f"\t\t\t\tDomicilio: {_p['nombre']}"

              f"\n\t\tFono: {_p['nombre']} " )

    def imprimir_info_acompanante(self, _p):
        if _p['tiene_compañia']:
            print(
                f"\t\tAsiste acompañado:         SI\n"
                f"\t\t______________________________________________________________\n"
                f"\t\t\t\tINFORMACION DE ACOMPAÑANTE\n"
                f"\t\t______________________________________________________________\n"
                f"\t\tNombre: {_p['nombre_acompanante']}\n"
                f"\t\tApellido: {_p['apellido_acompanante']}\n"
                f"\t\tRut: {formateo_rut(_p['rut_acompanante'])}\n"
                f"\t\tGrado de parentesco: {_p['grado_parentesco']}\t\t\t\tFono:  {_p['telefono_acompanante']}"
            )
        else:
            print(
                f"\t\tAsiste acompañado:             NO"
            )

    def imprimir_info_atencion(self, _p):
        print(
            f"\t\t______________________________________________________________\n"
            f"\t\t\t\tINFORMACION DE ATENCION\n"
            f"\t\t______________________________________________________________\n"
            f"\t\tNombre médico: {_p['nombre_medico']}\n"
            f"\t\tEspecialidad: {_p['especialidad_medico']}\n"
            f"\t\tSintomas detectados: {_p['sintomas_detectados']}")
        if _p['indica_reposo']:
            print(f"\t\tReposo:   SI"
                  f"\t\t\t\t\tCantidad de dias: {_p['dias_reposo']}")
        else:
            print(f"\t\tReposo:     NO"
                  )

    def imprimir_info_medicamento(self, _p):
        print(f"\t\t______________________________________________________________\n"
              f'\t\t\t\tINFORMACIÓN DE MEDICAMENTO\n'
              f"\t\t______________________________________________________________\n")
        if _p['indica_medicamento']:
            print(f"\t\tMedico Asigna medicamento:       SI")
            print(f"\t\tNombre de medicamento: {_p['nombre_medicamento']}")
            print(f"\t\tDosis: {_p['dosis_medicamento']}")
            print(f"\t\tCantidad de días: {_p['dias_medicamento']}")
        else:
            print(f"\t\tMedico Asigna medicamento:                 NO")
def imprimir_todo(printer,_p):
    printer.imprimir_titulo_ficha(_p)
    printer.imprimir_info_paciente(_p)
    printer.imprimir_info_acompanante(_p)
    printer.imprimir_info_atencion(_p)
    printer.imprimir_info_medicamento(_p)




def ingreso_atencion():
    pedir('nombre_medico', info_atencion)
    pedir('especialidad_medico', info_atencion)
    pedir('diagnostico_medico', info_atencion)


#preguntas sobre acompañante
def ingreso_acompanante():
    if respuestas['tiene_compañia']:
        pedir('nombre_acompanante',preguntas_acompanante)
        pedir('apellido_acompanante', preguntas_acompanante)
        pedir_rut('rut_acompanante')
        pedir('grado_parentesco', preguntas_acompanante)
        pedir('telefono_acompanante', preguntas_acompanante)



##ingreso de paciente
def ingreso_paciente() :
    pedir_rut('rut')
    pedir('nombre',preguntas_paciente)
    pedir('apellido',preguntas_paciente)
    pedir('estado_civil',preguntas_paciente)
    pedir('domicilio', preguntas_paciente)
    pedir('telefono', preguntas_paciente)
    preguntar_acompanado()
    pedir('urgencia',preguntas_paciente)
    pedir('sexo',preguntas_paciente)
    pedir('edad',preguntas_paciente)
    ingreso_acompanante()
    pedir('nombre_medico',info_atencion)
    pedir('especialidad_medico',info_atencion)
    pedir('sintomas_detectados',info_atencion)
    pregunta_reposo()
    pregunta_medicamento()




    pacientes.append(respuestas)
    print(respuestas)
    imprimir_todo(printer,respuestas)

#preguntar medicamento

def pregunta_medicamento():
    medicamento = input("Indica algun medicamento para el paciente? (SI/NO)").lower()
    indica_medicamento = False

    if medicamento == 'si':
        indica_medicamento = True
        pedir('nombre_medicamento',info_atencion)
        pedir('dosis_medicamento', info_atencion)
        pedir('dias_medicamento', info_atencion)
    if medicamento == 'no':
        indica_medicamento = False
    else:
        print('porfavor indicar (SI/NO)')
    respuestas['medicamento'] = medicamento
    respuestas['indica_medicamento'] = indica_medicamento

#preguntar reposo

def pregunta_reposo():
    reposo = input("Indica reposo para el paciente? (SI/NO)").lower()
    indica_reposo = False
    if reposo == 'si':
        indica_reposo = True
        pedir('dias_reposo',info_atencion)
    elif reposo == 'no':
        indica_reposo = False
    else:
        print('porfavor indique SI/NO')
        pregunta_reposo()
    respuestas['reposo'] = reposo
    respuestas['indica_reposo'] = indica_reposo

#paciente_acompañado?:
def preguntar_acompanado():
    for ea in preguntas_paciente:
        if ea['pregunta'] == 'acompanado':
            acompanado = input(f"{ea['print']}").lower()
            tiene_compañia = False
            if acompanado == "si":
                tiene_compañia = True

            elif acompanado == "no":
                tiene_compañia = False
            else:
                print("porfavor ,responda (SI/NO)\n")
                preguntar_acompanado()
    respuestas['acompanado'] = acompanado
    respuestas['tiene_compañia'] = tiene_compañia




#fx formatear rut:
def formateo_rut(rut):

    ru = rut[:-1]
    dv = rut[-1]
    ur = ru[::-1]
    partes = [ur[i:i+3] for i in range(0, len(ru), 3)]
    new = '.'.join(partes)
    return (f"{new[::-1]}-{dv}")
#verificar rut
def verificar_rut(cual,rut):

    for ea in pacientes:
        if ea[cual] == rut:

            return True




#fx pedir nombre:
def pedir(cual,set):
    for pregunta in set:
        if pregunta['pregunta']== cual:
            print(f"{pregunta['print']}")
            respuestas[cual] = input()

##fxs especificas
def pedir_rut(cual):
    rut = input('Ingrese RUT:')
    if len(rut) < 3:
        print('Porfavor ingresar un RUT valido \n')
        pedir_rut(cual)
    if verificar_rut(cual,rut):
            print(f'el rut {formateo_rut(rut)} ya está en la base de datos')
            return
    else: respuestas[cual] = rut




##Preguntas sobre el paciente

preguntas_paciente = [{
        'pregunta': 'rut',
         'print': 'Ingrese el RUT ',
         'tabla': 'Rut:',
                         }
                         ,{
        'pregunta': 'nombre',
                      'print': 'Ingrese el Nombre: ',
        'tabla': 'Nombre:',

    }, {
        'pregunta': 'apellido',
        'print': 'Ingrese el apellido: ',
        'tabla': 'Apellido:',

    },  {
        'pregunta': 'estado_civil',
        'print': 'Ingrese el Estado Civil: ',
        'tabla': 'Estado Civil'
    }, {
        'pregunta': 'domicilio',
        'print': 'Ingrese el domicilio:',
        'tabla': 'Domicilio:'
    }, {
        'pregunta': 'telefono',
        'print': 'Ingrese el Telefono de contacto:',
        'tabla': 'Fono:'
    }, {
        'pregunta': 'acompanado',
        'print': 'El Paciente viene acompañado? (SI/NO)',
        'tabla': 'Asiste acompañado:'
    }, {
        'pregunta': 'urgencia',
        'print': 'Nivel de Urgencia:',
        'tabla': 'Nivel de urgencia :'
    }, {
        'pregunta': 'sexo',
        'print': 'Sexo del paciente:',
        'tabla': 'Sexo :'
    }, {
        'pregunta': 'edad',
        'print': 'Edad del paciente:',
        'tabla': 'Edad :'
    }, {
        'pregunta': 'grupo_sanguineo',
        'print': 'Grupo sanguineo del paciente:',
        'tabla': 'Grupo sanguineo :'
    },
]

##Preguntas sobre el acompañante

preguntas_acompanante = [
    {
        'pregunta': 'nombre_acompanante',
        'print': 'Nombre del acompañante: ',
        'tabla': 'Nombre:'
    }, {
        'pregunta': 'apellido_acompanante',
        'print': 'Apellido del acompañante: ',
        'tabla': 'Apellido:'
    }, {
        'pregunta': 'rut_acompanante',
        'print': 'RUT del acompañante: ',
        'tabla': 'RUT:'
    }, {
        'pregunta': 'grado_parentesco',
        'print': 'Indique el Grado de Parentesco: ',
        'tabla': 'Grado de Parentesco:'
    }, {
        'pregunta': 'telefono_acompanante',
        'print': 'Ingrese el Telefono: ',
        'tabla': 'Fono:'
    }]

##
##IFORMACION DE ATENCION
info_atencion = [
    {
        'pregunta': 'nombre_medico',
        'print': 'Nombre del medico que atiende al paciente: ',
        'tabla': 'Nombre:'
    }, {
        'pregunta': 'especialidad_medico',
        'print': 'Especialidad del medico: ',
        'tabla': 'Apellido:'
    }, {
        'pregunta': 'sintomas_detectados',
        'print': 'Sintomas detectados: ',
        'tabla': 'Sintomas detectados:'
    }, {
        'pregunta': 'diagnostico_medico',
        'print': 'Ingrese diagnostico: ',
        'tabla': 'Diagnóstico:'
    }, {
        'pregunta': 'reposo',
        'print': 'Se indica reposo para el paciente? ',
        'tabla': 'Reposo:'
    }, {
        'pregunta': 'dias_reposo',
        'print': 'Cuantos dias? ',
        'tabla': 'Cantidad de dias:'
    }, {
        'pregunta': 'asigna_medicamento',
        'print': 'Asigna algun medicamento al paciente? ',
        'tabla': 'Médico Asigna medicamento:'
    }, {
        'pregunta': 'nombre_medicamento',
        'print': 'Que medicamento deberia tomar? ',
        'tabla': 'Nombre de medicamento:'
    }, {
        'pregunta': 'dosis_medicamento',
        'print': 'Que dosis deberia tomar? ',
        'tabla': 'Dosis:'
    }, {
        'pregunta': 'dias_medicamento',
        'print': 'Por cuantos dias?',
        'tabla': 'Cantidad de días:'
    }]

##Variable respuestas, almacena los datos colectados


respuestas = {}
#Variables fecha y hora
now_h = dt.utcnow()
now = dt.now()
f_str = (now.strftime("%H/%m/%y"))

h_str = (f"{now_h.hour-3}:{now_h.minute}")



#Lista de pacientes:
pacientes = []
printer = Printer()


#MENU



# Greeter is a terminal application that greets old friends warmly,
#   and remembers new friends.


### FUNCTIONS ###

def display_titulo():
    # Clears the terminal screen, and displays a title bar.
    os.system('clear')

    print("\t**********************************************")
    print("\t\t   ***  Centro de Salud DUOC UC ***")
    print("\t**********************************************")


def opcion_elegida():
    # Let users know what they can do.
    print("\n[1] Fichas registradas.")
    print("[2] Ingreso de Paciente.")
    print("[x] Salir.")

    return input("Ingrese accion a realizar:   ")


def mostrar_nombres():
    # Shows the names of everyone who is already in the list.
        if len(pacientes) > 0:
            print("Esta es la lista de pacientes registrados:")
            for _p in pacientes:
                print(f"[{pacientes.index(_p) + 1}]   {_p['apellido']}/{formateo_rut(_p['rut'])}")

        else:
            print("Aun no existen registros de pacientes")







### MAIN PROGRAM ###

# Set up a loop where users can choose what they'd like to do.


opcion = ''
display_titulo()
while opcion != 'x':

    opcion = opcion_elegida()

    # Respond to the user's choice.
    display_titulo()
    if opcion == '1':
        mostrar_nombres()
    elif opcion == '2':
        ingreso_paciente()
    elif opcion == 'x':
        print("\nGracias por usar el sistema. Adios")
    else:
        print("\nPorfavor ingrese un comando valido\n")




#ingreso de paciente nuevo:









