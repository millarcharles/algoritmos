import datetime
from datetime import datetime as dt
ahora = dt.now()# dd/mm/YY H:M:S
dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
print("date and time =", dt_string)

#fx formatear rut:
def formateo_rut(rut):

    ru = rut[:-1]
    dv = rut[-1]
    ur = ru[::-1]
    partes = [ur[i:i+3] for i in range(0, len(ru), 3)]
    new = '.'.join(partes)
    return (f"{new[::-1]}-{dv}")
#verificar rut
def verificar_rut(rut):

    for ea in pacientes:
        if ea['rut'] == rut:

            return True


##class printer, almacena las functiones que dan formato a la ficha
class Printer():
    def imprimir_titulo_ficha(self,_p):
        print(
            f"\t\t*************************************************************\n"
            f"\t\t             ***  Centro medico DUOC UC ***\n"
            f"\t\t______________________________________________________________\n"
            f"\t\t                      FICHA Nº XXXX                           \n"
            f"\t\t______________________________________________________________\n"

            f"\t\tFecha de atención:XX/XX/XXXX    |    Hora de Atención: HH:mm\n"
            f"\t\t______________________________________________________________\n")

    def imprimir_info_paciente(self,_p):
        print(f"\t\t\t\t\t\t\tINFORMACION PACIENTE\n"
              f"\t\t______________________________________________________________\n"
              f"\t\tNombre: {_p['nombre']}\t\t      \t\t\t\t\tNivel de urgencia:   {_p['urgencia']}\n"
              f"\t\tApellido: {_p['nombre']}                            RUT: {formateo_rut(_p['rut'])}\n"
              f"\t\tEstado Civil: {_p['nombre']}"f"\t\t\t\t\t\tDomicilio: {_p['nombre']}"

              f"\n\t\tFono: {_p['nombre']} " )

    def imprimir_info_acompanante(self, _p):
        if _p['tiene_compañia']:
            print(
                f"\t\tAsiste acompañado:         SI\n"
                f"\t\t______________________________________________________________\n"
                f"\t\t\t\t\t\tINFORMACION DE ACOMPAÑANTE\n"
                f"\t\t______________________________________________________________\n"
                f"\t\tNombre: {_p['nombre_acompanante']}\n"
                f"\t\tApellido: {_p['apellido_acompanante']}\n"
                f"\t\tRut: {formateo_rut(_p['rut_acompanante'])}\n"
                f"\t\tGrado de parentesco: {_p['grado_parentesco']}\t\t\t\tFono:  {_p['telefono_acompanante']}"
            )
        else:
            print(
                f"\t\tAsiste acompañado:             NO"
            )

    def imprimir_info_atencion(self, _p):
        print(
            f"\t\t______________________________________________________________\n"
            f"\t\t\t\t\t\tINFORMACION DE ATENCION\n"
            f"\t\t______________________________________________________________\n"
            f"\t\tNombre médico: {_p['nombre_medico']}\n"
            f"\t\tEspecialidad: {_p['especialidad_medico']}\n"
            f"\t\tSintomas detectados: {_p['sintomas_detectados']}")

        if _p['indica_reposo']:
            print(f"\t\tReposo:   SI"
                  f"\t\t\t\t\t\t\tCantidad de dias: {_p['dias_reposo']}")
        else:
            print(f"\t\tReposo:     NO"
                  )

    def imprimir_info_medicamento(self, _p):
        print(f"\t\t______________________________________________________________\n"
              f'\t\t\t\t\t\tINFORMACIÓN DE MEDICAMENTO\n'
              f"\t\t______________________________________________________________\n")
        if _p['indica_medicamento']:
            print(f"\t\tMedico Asigna medicamento:       SI")
            print(f"\t\tNombre de medicamento: {_p['nombre_medicamento']}")
            print(f"\t\tDosis: {_p['dosis_medicamento']}")
            print(f"\t\tCantidad de días: {_p['dias_medicamento']}")
        else:
            print(f"\t\tMedico Asigna medicamento:                 NO")
def imprimir_todo(printer,_p):
    printer.imprimir_titulo_ficha(_p)
    printer.imprimir_info_paciente(_p)
    printer.imprimir_info_acompanante(_p)
    printer.imprimir_info_atencion(_p)
    printer.imprimir_info_medicamento(_p)


_p = {'rut': '23131223', 'nombre': 'ca', 'apellido': 'millar', 'estado_civil': 'cas', 'domicilio': 'masd', 'telefono': '12332', 'acompanado': 'si', 'tiene_compañia': False, 'urgencia': '2', 'sexo': 'ma', 'edad': '213', 'nombre_acompanante': 'dsaasd', 'apellido_acompanante': 'dsa', 'rut_acompanante': '21312', 'grado_parentesco': 'polola', 'telefono_acompanante': '132312', 'nombre_medico': 'dr', 'especialidad_medico': 'ma', 'sintomas_detectados': 'tos', 'dias_reposo': '2', 'reposo': 'si', 'indica_reposo': True, 'nombre_medicamento': 'paracetamol', 'dosis_medicamento': '40', 'dias_medicamento': '2', 'medicamento': 'si', 'indica_medicamento': False}
printer = Printer()
imprimir_todo(printer,_p)